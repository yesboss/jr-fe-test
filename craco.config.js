const path = require('path');
const reactHotReloadPlugin = require('craco-plugin-react-hot-reload');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { paths } = require('@craco/craco');

module.exports = {
  plugins: [
    {
      plugin: reactHotReloadPlugin
    }
  ],
  webpack: {
    alias: {
      '~': `${paths.appSrc}/`
    },
    configure: (config, { env, paths }) => {
      // Add `webpack-bundle-analyzer` if `ANALYZE` env is set.
      if (env === 'production' && process.env.ANALYZE) {
        config.plugins.push(
          new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: path.join(__dirname, '../webpack-bundle-analyzer/report.html')
          })
        );
      }

      return config;
    }
  },
  jest: {
    configure: {
      moduleNameMapper: {
        '^~(.*)$': '<rootDir>/src$1'
      }
    }
  },
  babel: {
    plugins: ['babel-plugin-styled-components']
  }
};
